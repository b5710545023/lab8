package pizzashop.food;

import java.util.List;
import java.util.ArrayList;
/**
 * Pizza is the class that inheritance of abstractitem
 * @author Kundjanasith Thonglek
 * @version 10.03.2015
 */
public class Pizza extends AbstractItem{
	/** prices is the static final array of double that show price of Pizza	 */
	public static final double [] prices = { 0.0, 120.0, 200.0, 300.0 };
	/** toppings is the List of topping  */
	private List<Topping> toppings;
	/**
	 *Constructor of the pizza 
	 * @param size is 1 for small, 2 for medium, 3 for large.
	 */
	public Pizza( int size ) {
		super(size);
		toppings = new ArrayList<Topping>();
	}
	/**
	 *  toString is the method that create String the description of Pizza
	 *  @return String description of this pizza 
	 */
	public String toString() {
		StringBuffer tops = new StringBuffer();
		if ( toppings.size() == 0 || 
				(toppings.size()==1 && toppings.get(0) == Topping.NONE ) ) ;
		else {
			tops.append(" with ");
			tops.append(toppings.get(0).toString());
			for( int k = 1; k<toppings.size(); k++ ) {
				tops.append(", ");
				tops.append( toppings.get(k).toString() );
			}
		}

		return String.format("%s pizza%s", sizes[getSize()], tops.toString() );
	}
	/**
	 *  getPrice is the method to get the price of pizza
	 *  @see pizzashop.FoodItem#getPrice() 
	 *  @return the price of Pizza
	 */
	public double getPrice() {
		double price = 0;
		if ( getSize() >= 0 && getSize() < prices.length ) price += prices[getSize()];
		for(Topping top : toppings) price+=top.getPrice();
		return price;
	}
	/** 
	 * add a topping
	 * @param topping is a topping to add to pizza
	 */
	public void add(Topping topping) {
		toppings.add( topping );
	}
	/**
	 * remove a topping
	 * @param topping is a topping to add to pizza
	 */
	public void remove(Topping topping) {
		toppings.remove(topping);
	}
	/**
	 * getToppings is the method to get the all of topping of Pizza
	 * @return toppings is the List of topping
	 */
	public List<Topping> getToppings( ) {
		return toppings;
	}
	/**
	 * setToppiings is the method to set the all of topping of Pizza
	 * @param toppings is the List of topping
	 */
	public void setToppings( List<Topping> toppings ) {
		this.toppings = toppings;
	}
	/**
	 * clone is the method that use for copy another Pizza
	 * @return Object is copied
	 */
	public Object clone() {
		Pizza clone = null;
		try {
			clone = (Pizza) super.clone();
			clone.setSize(getSize());
			// copy toppings into a new List in clone
			clone.toppings = new ArrayList<Topping>();
			for(Topping t : toppings ) clone.toppings.add(t);
		} catch (CloneNotSupportedException e) {
			System.out.println("Pizza.clone: " + e.getMessage());
		}

		return clone;
	}
}
