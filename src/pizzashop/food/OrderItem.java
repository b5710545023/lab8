package pizzashop.food;
/**
 * OrderItem is the interface 
 * @author Kundjanasith Thonglek
 * @version 10.03.2015
 */
public interface OrderItem {
	double getPrice();
}
