package pizzashop.food;
/**
 * topping is the enum that contain the topping of Pizza
 * @author Kundjanasith Thonglek
 * @version 10.03.2015
 */
public enum Topping {
	VEGETABLE("vegetable", 15),
	MUSHROOM("mushroom", 15),
	SAUSAGE("sausage", 20),
	PINEAPPLE("pineapple", 10),
	TOFU("tofu", 15),
	NONE("none", 0);
	/** name is the String of the name of topping	 */
	private String name;
	/** price is the price of the topping */
	private double price;
	/**
	 * Constructor of topping
	 * @param name of topping
	 * @param price of topping
	 */
	private Topping(String name, double price) {
		this.name = name;
		this.price = price;
	}
	/**
	 * Constuctor of topping
	 * @param name of toppping
	 */
	private Topping(String name) {
		this.name = name;
	}
	/** 
	 * toString is the method that show the description of topping 
	 * @return description of the topping 
	 */
	public String toString() { return this.name; }
	/** 
	 * getPrice is the method that get the price of topping
	 * @return the topping price 
	 */
	public double getPrice() { return this.price; }
}
