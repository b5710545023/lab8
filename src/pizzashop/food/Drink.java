package pizzashop.food;
/**
 * Drink is class that inherit from abstract item
 * @author Kundjanasith Thonglek
 * @version 10.03.2015
 */
public class Drink extends AbstractItem{
	/** prices is the static final array of double that show about price of drink  */
	public static final double [] prices = { 0.0, 20.0, 30.0, 40.0 };
	/** flavor is the String that show the flavor of this drink */
	private String flavor;
	/**
	 * constructor of drink class
	 * create a new drink.
	 * @param size is the size. 1=small, 2=medium, 3=large
	 */
	public Drink( int size ) {
		super(size);
	}
	/*  
	 * toString is the method that show description of Drink
	 * @return the description of Drink
	 */
	public String toString() {
		return sizes[getSize()] + " " + (flavor==null? "": flavor.toString()) + "drink"; 
	}
	/**
	 * getPrice is the method that get the Price of this Drink
	 *  @return the price of a drink
	 */
	public double getPrice() {
		if (getSize() >= 0 &&getSize() < prices.length) return prices[getSize()];
		return 0.0;
	}
	/**
	 * clone is the method that use for copy another Drink
	 * @return Object is copied
	 */
	public Object clone() {
		Drink clone = null;
		try {
			clone = (Drink) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("Drink.clone: " + e.getMessage());
		}
		return clone;
	}
}
