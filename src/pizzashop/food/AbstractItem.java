package pizzashop.food;
/**
 * AbstracItem is the abstract class that realize OrderItem interface 
 * @author Kundjanasith Thonglek
 * @version 10.03.2015
 */
public abstract class AbstractItem implements OrderItem{
	/** sizes is static final array of String for store size of OrderItem */
	public static final String [] sizes = { "none", "small", "medium", "large" };
	/** size is integer that the size of the AbstractItem	 */
	private int size;
	/**
	 * Constructor of AbstractItem
	 * @param size is the size of the abstract item
	 */
	public AbstractItem(int size){
		this.size=size;
	}
	/**
	 * setSize is the method that set the size of abstract item
	 * @param size is the size of the abstract item
	 */
	public void setSize(int size){
		this.size=size;
	}
	/**
	 * getSize is the method that get the size of abstract item
	 * @return size is the size of the abstract item
	 */
	public int getSize(){
		return this.size;
	}
}
