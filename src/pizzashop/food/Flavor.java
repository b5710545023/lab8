package pizzashop.food;
/**
 * flavor is the enum that contain the flavor of Drink
 * @author Kundjanasith Thonglek
 * @version 10.03.2015
 */
public enum Flavor {
	COKE("Coke"),
	PEPSI("Pepsi"),
	SPRITE("Sprite"),
	ORANGE("Orange"),
	WATER("Water"),
	COFFEE("Coffee");
	/** name is the String that show the name of flavor */
	private String name;
	/**
	 * constructor of flavor 
	 * @param name is the name of this enum flavor
	 */
	Flavor(String name) { 
		this.name = name; 
	}
	/**
	 * toString is the method to show the name of flavor
	 * @return name of flavor
	 */
	public String toString() { 
		return name;
	}
}
