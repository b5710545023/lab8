package pizzashop;
import java.util.*;
/**
 * PizzaStore is the class for Store of Pizza
 * @author Kundjanasith Thonglek
 * @version 10.03.2015
 */
public class PizzaStore {
	/** order is the List of FoodOrder */
	List<FoodOrder> orders;
	/** the Store is the static PizzaStore create to null */
	private static PizzaStore theStore = null; 
	/**
	 * constructor of PizzaStore
	 */
	private PizzaStore() {
		orders = new ArrayList<FoodOrder>();
	}
	/** 
	 * addOrder is the method that Submit a food order for processing. 
	 */
	public void addOrder(FoodOrder fo ) { 
		orders.add( fo ); 
	}
	/** 
	 * removeOrder is the method that Remove a food order.
	 */
	public void removeOrder(FoodOrder fo ) { 
		orders.remove( fo ); 
	}
	/**
	 *  getInstance is the method that accessor to get the pizza store.
	 */
	public static PizzaStore getInstance( ) {
		if ( theStore == null ) theStore = new PizzaStore( );
		return theStore;
	}
	/**
	 *  getOrders is the method that List all the food order.
	 */
	public List<FoodOrder> getOrders() {
		return orders;
	}
}
