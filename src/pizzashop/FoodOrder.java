package pizzashop;
import pizzashop.food.*;
import java.util.List;
import java.util.ArrayList;
/**
 * FoodOrder 
 * @author Kundjanasith Thonglek
 * @vaersion 10.03.2015
 */

public class FoodOrder {
	/** items is List of OrderItem */
	private List<OrderItem> items;
    /**
     * Constructor of foodOrder
     * create the ArrayList of OrderItem 
     */
	public FoodOrder() {
		items = new ArrayList<OrderItem>( );
	}
	/**
	 * addOrderItem is the method for add Item to the Ordered Item
	 * @param orderitem is OrderItem is added
	 */
	public void addOrderItem(OrderItem orderitem){
		items.add(orderitem);
	}
	/**
	 * getTotal is the method for get total price
	 * @return the total cost of the price od oredered item
	 */
	public double getTotal() {
		double total = 0;
		for(OrderItem i: items) total += i.getPrice();
		return total;
	}
	/**
	 * printOrder is the method to print show the String of items
	 */
	public void printOrder() {
		for(OrderItem i: items) System.out.println(i.toString());
		System.out.println("Total price: " + getTotal() );
	}
}
