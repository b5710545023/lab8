package pizzashop;
import pizzashop.food.Drink;
import pizzashop.food.Pizza;
import pizzashop.food.Topping;
import static java.lang.System.out;
/**
 * Main class for runnable this application
 * @author Kundjanasith Thonglek
 * @version 10.03.2015
 */
public class Main {
    /**
     * testDrink is the method for test drink
     * @return the Drink 
     */
	public static Drink testDrink() {
		out.println("\nTesting Drink class");
		Drink d = new Drink( 1 ); // small
		out.println("should be: small drink");
		out.println("your code: "+d.toString());
		out.println("should be: "+Drink.prices[1]);
		out.println("your code: "+d.getPrice());
		d = new Drink( 3 ); // large
		out.println("should be: large drink");
		out.println("your code: "+d.toString());
		out.println("should be: "+Drink.prices[3]);
		out.println("your code: "+d.getPrice());
		return d;
	}
	/**
     * testPizza is the method for test Pizza
     * @return the Pizza
     */	
	public static Pizza testPizza() {
		out.println("\nTesting Pizza class");
		out.println("making a small pizza...");
		Pizza pizza = new Pizza( 1 );
		double price = Pizza.prices[1];
		out.println("should be: small pizza");
		out.println("your code: "+pizza.toString());
		out.println("getPrice should be: " + price);
		out.println("getPrice your code: "+pizza.getPrice());
		out.println("adding toppings...");
		pizza.add( Topping.PINEAPPLE );
		pizza.add( Topping.MUSHROOM );
		price += Topping.PINEAPPLE.getPrice() + Topping.MUSHROOM.getPrice();
		out.println("should be: small pizza with pineapple, mushroom");
		out.println("your code: "+pizza.toString());
		out.println("getPrice should be: " + price);
		out.println("getPrice your code: "+pizza.getPrice());
		return pizza;
	}
	/**
	 * main is the method for runnable class of this application
	 * @param args is the array of String  
	 */
	public static void main(String[] args) {
		
		try {
			Drink drink = testDrink();
			Pizza pizza = testPizza();
			FoodOrder order = new FoodOrder();
			order.addOrderItem(drink);
			order.addOrderItem(pizza);
			// submit the order
			PizzaStore store = PizzaStore.getInstance();
			store.addOrder( order );
			
		} catch (Exception e) {
			out.println("code threw exeption: "+e.getMessage() );
			e.printStackTrace();
		}
	}
}
